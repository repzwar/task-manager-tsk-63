package ru.pisarev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public class Task {

    @NotNull
    protected String id = UUID.randomUUID().toString();

    @NotNull
    private String name;

    @Nullable
    private String description;

    @Nullable
    private String projectId;

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @NotNull
    private Date created = new Date();

    @NotNull
    public Task(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public Task(@NotNull String name, @Nullable String description) {
        this.name = name;
        this.description = description;
    }

}
