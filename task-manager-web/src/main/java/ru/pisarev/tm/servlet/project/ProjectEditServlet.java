package ru.pisarev.tm.servlet.project;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/edit/*")
public class ProjectEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final Project project = ProjectRepository.getInstance().findById(id);
        req.setAttribute("project", project);
        req.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final String name = req.getParameter("name");
        @NotNull final String description = req.getParameter("description");
        @NotNull final Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        ProjectRepository.getInstance().save(project);
        resp.sendRedirect("/projects");
    }
}
