package ru.pisarev.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Getter
public class ProjectRepository {

    @NotNull
    private static final ProjectRepository INSTANCE = new ProjectRepository();

    @NotNull
    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        save(new Project("Alpha"));
        save(new Project("Betta"));
        save(new Project("Gamma", "Epsilon"));
    }

    public void create() {
        save(new Project("Project" + System.currentTimeMillis()));
    }

    public void save(@NotNull Project project) {
        projects.put(project.getId(), project);
    }

    @NotNull
    public Collection<Project> findAll() {
        return projects.values();
    }

    @Nullable
    public Project findById(@NotNull String id) {
        return projects.get(id);
    }

    public void removeById(@Nullable String id) {
        projects.remove(id);
    }

}
