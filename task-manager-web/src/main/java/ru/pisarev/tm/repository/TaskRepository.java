package ru.pisarev.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Getter
public class TaskRepository {

    @NotNull
    private static final TaskRepository INSTANCE = new TaskRepository();

    @NotNull
    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        save(new Task("Alpha"));
        save(new Task("Betta"));
        save(new Task("Gamma", "Epsilon"));
    }

    public void create() {
        save(new Task("Task" + System.currentTimeMillis()));
    }

    public void save(@NotNull Task task) {
        tasks.put(task.getId(), task);
    }

    @NotNull
    public Collection<Task> findAll() {
        return tasks.values();
    }

    @Nullable
    public Task findById(@NotNull String id) {
        return tasks.get(id);
    }

    public void removeById(@Nullable String id) {
        tasks.remove(id);
    }

}
