package ru.pisarev.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.pisarev.tm.component.Bootstrap;
import ru.pisarev.tm.config.LoggerConfig;

public class Application {

    @SneakyThrows
    public static void main(final String[] args) {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(LoggerConfig.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start();
    }

}